# Klausimelis

## Overview
Online assessment tool.

* Current state: Prototype. Not intended for actual usage.

### Technical details

* Uses in-memory persistence for rapid development. In case of real usage, this should be replaced.
* Stack: Node, Express, NeDb, JSPM, React, Redux...



## Development

### Requiremenets

* Node 6.0+


### How To

**PROD**

No hot-reload and front-end is bundled.

1. `npm install`
2. `npm start`
3. Visit [http://localhost:8080](http://localhost:8080/)

**DEV**

With hot-reload for back-end and front-end, not bundled. Also initialized with sample data.

1. `npm install`
2. `npm run start:dev`
3. Visit [http://localhost:8080](http://localhost:8080/)
