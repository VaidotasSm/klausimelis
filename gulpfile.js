const gulp = require('gulp');
const runSequence = require('run-sequence');
const nodemon = require('gulp-nodemon');
const eslint = require('gulp-eslint');
const mocha = require('gulp-mocha');
const del = require('del');
const exec = require('child_process').exec;
const htmlreplace = require('gulp-html-replace');

gulp.task('build', function (callback) {
    return runSequence(
        'lint',
        'test',
        'clean',
        'bundle-js',
        'copy-resources',
        'process-html',
        callback
    )
});

gulp.task('lint', function () {
    return gulp.src([
        'app-api/src/**/*.js'
    ])
        .pipe(eslint({}))
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

gulp.task('test', function () {
    return gulp
        .src('app-api/test/**/*.js', { read: false })
        .pipe(mocha({ reporter: 'dot' }))
});

gulp.task('start', function () {
    nodemon({
        script: 'app-api/src/index.js',
        args: [],
        ext: 'js',
        ignore: ['./node_modules', './app-ui'],
        env: {
            PORT: 8080,
            PROFILE: 'dev'
        }
    }).on('restart', () => {
        console.log('Restarting…');
    });
});

gulp.task('clean', function () {
    return del(['app-ui/dist/**/*']);
});

gulp.task('bundle-js', (cb) => {
    exec('jspm bundle-sfx --minify --no-mangle src/main.js app-ui/dest/main.bundle.js', function (err, stdout, stderr) {
        cb(err);
    });
});

gulp.task('copy-resources', function () {
    gulp.src('app-ui/jspm_packages/npm/bootswatch@3.3.7/flatly/bootstrap.min.css')
        .pipe(gulp.dest('app-ui/dest'));
    gulp.src('app-ui/favicon.ico')
        .pipe(gulp.dest('app-ui/dest'));
});

gulp.task('process-html', function () {
    const timeStamp = new Date().getTime();
    return gulp.src('app-ui/index.html')
        .pipe(htmlreplace({
            'js-replace': `main.bundle.js?version=${timeStamp}`,
            'css-replace': `bootstrap.min.css`
        }))
        .pipe(gulp.dest('app-ui/dest'));
});
