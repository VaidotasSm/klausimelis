const expect = require('chai').expect;
const supertest = require("supertest-as-promised");

const AppServer = require('../../src/server/server');
const DevPersistence = require('../../src/plugins/impl/dev.persistence.js');
const testModel = require('../../src/business/question');
const assessmentModel = require('../../src/business/assessment');

const TESTS_PATH = '/api/tests';

let agent;
const server = new AppServer(new DevPersistence(), 'dev', 8080);

before(done => {
    server.start();
    agent = supertest.agent(server.app);
    done();
});

after(done => {
    server.handle.close();
    done();
});

describe('Tests management: ', function () {
    const question1 = new testModel.Question('2 x 3 = ?')
        .addAnswer(new testModel.Answer('6', true))
        .addAnswer(new testModel.Answer('3'));
    const question2 = new testModel.Question('a?')
        .addAnswer(new testModel.Answer('a', true))
        .addAnswer(new testModel.Answer('b'));
    const question3 = new testModel.Question('b?')
        .addAnswer(new testModel.Answer('a'))
        .addAnswer(new testModel.Answer('b', true));

    it('Retrieves all tests', done => {
        const test1 = new testModel.TestWrapper('Retrieves all tests - 1')
            .setCurrent(new testModel.Test().addQuestion(question2));
        const test2 = new testModel.TestWrapper('Retrieves all tests - 2')
            .setCurrent(new testModel.Test().addQuestion(question3));

        agent.post(TESTS_PATH).send(test1).expect(201)
            .then(() => agent.post(TESTS_PATH).send(test2).expect(201))
            .then(() => agent.get(TESTS_PATH)
                .expect(200)
                .then(res => {
                    expect(res.body.entries.length).to.equal(2);
                    expect(res.body._metadata.totalCount).to.equal(2);
                    expect(res.body._links).to.exist;
                }))
            .then(() => done())
            .catch(err => done(err));
    });

    it('Can create and retrieve test', done => {
        const test1 = new testModel.TestWrapper('Can create and retrieve test')
            .setCurrent(new testModel.Test().addQuestion(question1));

        agent.post(TESTS_PATH).send(test1)
            .expect(201)
            .then(res => {
                expect(res.header.location).to.have.string('/api/tests/');
                return parseId(res.header.location);
            })
            .then(id => agent.get(`${TESTS_PATH}/${id}`)
                .expect(200)
                .then(res => {
                    expect(res.body.id).to.equal(id);
                    expect(res.body._id).to.not.exist;
                    expect(res.body.current.version).to.equal(1);
                })
            )
            .then(() => done())
            .catch(err => done(err));
    });

    it('Can update test and archive old test versions', done => {
        const test1 = new testModel.TestWrapper('Can update test')
            .setCurrent(new testModel.Test().addQuestion(question1));

        agent.post(TESTS_PATH).send(test1)
            .expect(201)
            .then(res => parseId(res.header.location))
            .then(id => {
                return agent.patch(`${TESTS_PATH}/${id}`)
                    .send({
                        name: 'Can update test - e1',
                        current: new testModel.Test().addQuestion(question1).addQuestion(question2)
                    })
                    .expect(200)
                    .then(() => agent.get(`${TESTS_PATH}/${id}`)
                        .expect(200)
                        .then(res => {
                            expect(res.body.id).to.equal(id);
                            expect(res.body.name).to.equal('Can update test - e1');
                            expect(res.body.current.version).to.equal(2);
                            expect(res.body.current.questions.length).to.equal(2);
                            expect(res.body.archive.length).to.equal(1);
                            expect(res.body.archive[0].version).to.equal(1);
                            expect(res.body.archive[0].questions.length).to.equal(1);
                        })
                    );
            })
            .then(() => done())
            .catch(err => done(err));
    });

    it('404 when no test exists', done => {
        agent.get(`${TESTS_PATH}/sdasafdsfdsfdsf`)
            .expect(404)
            .then(() => done())
            .catch(err => done(err));
    });
});

describe('Assessment management: ', function () {
    let suiteTestId = null;
    let suiteAssessmentId = null;

    before(done => {
        const test1 = new testModel.TestWrapper('Test 1')
            .setCurrent(new testModel.Test()
                .addQuestion(new testModel.Question('2 x 3 = ?')
                    .addAnswer(new testModel.Answer('6', true))
                    .addAnswer(new testModel.Answer('3'))));

        agent.post(TESTS_PATH).send(test1)
            .expect(201)
            .then(res => parseId(res.header.location))
            .then(testId => agent.post(`${TESTS_PATH}/${testId}/assessments`)
                .send(new assessmentModel.Assessment('Assessment 1', testId))
                .expect(201)
                .then(res => {
                    suiteTestId = testId;
                    suiteAssessmentId = parseId(res.header.location)
                })
            )
            .then(() => done())
            .catch(done);
    });

    it('Can view assessment in list', done => {
        agent.get(`${TESTS_PATH}/${suiteTestId}/assessments`)
            .expect(200)
            .then(res => {
                expect(res.body.entries.length).to.equal(1);
                expect(res.body.entries[0].name).to.equal('Assessment 1');
            })
            .then(() => done())
            .catch(done);
    });

    it('Can view assessment by id', done => {
        agent.get(`${TESTS_PATH}/${suiteTestId}/assessments/${suiteAssessmentId}`)
            .expect(200)
            .then(res => {
                expect(res.body.name).to.equal('Assessment 1');
                expect(res.body.testId).to.equal(suiteTestId);
                expect(res.body.submission.submitted).to.equal(false);
                expect(res.body.submission.questions.length).to.equal(0);
                expect(res.body._links.participate).to.have.string(`/api/public/assessments/${suiteTestId}/${suiteAssessmentId}`);
            })
            .then(() => done())
            .catch(done);
    });

    it('Can retrieve assessment submission', done => {
        agent.get(`/api/public/assessments/${suiteTestId}/${suiteAssessmentId}`)
            .expect(200)
            .then(res => {
                expect(res.body.name).to.equal('Assessment 1');
                expect(res.body.testId).to.equal(suiteTestId);
                expect(res.body.submission.submitted).to.equal(false);
                expect(res.body.submission.testVersion).to.not.exist;
                expect(res.body.submission.questions.length).to.equal(0);
                expect(res.body._embedded.test.name).to.equal('Test 1');
                expect(res.body._embedded.test.archive).to.not.exist;
                expect(res.body._embedded.test.content.questions.length).to.equal(1);
                expect(res.body._embedded.test.content.questions[0].answers[0].correct).to.not.exist;
                expect(res.body._embedded.test.content.questions[0].answers[1].correct).to.not.exist;
            })
            .then(() => done())
            .catch(done);
    });

    it('Assessment submission is visible and cannot be resubmitted', done => {
        const url = `/api/public/assessments/${suiteTestId}/${suiteAssessmentId}`;
        let questions;
        agent.get(url)
            .expect(200)
            .then(res => {
                questions = [...res.body._embedded.test.content.questions];
                questions.forEach(q => q.answer = q.answers[0].value);
                return agent.patch(`/api/public/assessments/${suiteTestId}/${suiteAssessmentId}`)
                    .send({submission: {questions}})
                    .expect(200);
            })
            .then(() => agent.get(`${TESTS_PATH}/${suiteTestId}/assessments/${suiteAssessmentId}`)
                .expect(200)
                .then(res => {
                    expect(res.body.submission.submitted).to.equal(true);
                    expect(res.body.submission.testVersion).to.equal(1);
                    expect(res.body.submission.questions.length).to.equal(1);
                    expect(res.body.submission.questions[0].answer).to.equal('6');
                })
            )
            .then(() => agent.patch(`/api/public/assessments/${suiteTestId}/${suiteAssessmentId}`)
                .send({submission: {questions}})
                .expect(404))
            .then(() => done())
            .catch(done);
    })
});

function parseId(locationHeader) {
    const id = locationHeader.slice(locationHeader.lastIndexOf('/') + 1, locationHeader.length);
    if (!id || id === 'undefined') {
        throw new Error(`Location header is invalid: ${id}`);
    }
    return id;
}
