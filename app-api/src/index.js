const log = require('./server/logger');
const AppServer = require('./server/server');
const DevPersistence = require('./plugins/impl/dev.persistence.js');
const testModel = require('./business/question');
const assessmentModel = require('./business/assessment');

const devPersistence = new DevPersistence();
if (process.env.PROFILE === 'dev') {
    const question1 = new testModel.Question('2 x 3 = ?')
        .addAnswer(new testModel.Answer('6', true))
        .addAnswer(new testModel.Answer('3'));
    const question2 = new testModel.Question('a?')
        .addAnswer(new testModel.Answer('a', true))
        .addAnswer(new testModel.Answer('b'));
    const question3 = new testModel.Question('b?')
        .addAnswer(new testModel.Answer('a'))
        .addAnswer(new testModel.Answer('b', true));
    const test1 = new testModel.TestWrapper('Sample Test 1')
        .setCurrent(new testModel.Test().addQuestion(question1).addQuestion(question2));
    const test2 = new testModel.TestWrapper('Sample Test 2')
        .setCurrent(new testModel.Test().addQuestion(question3).addQuestion(question1));

    Promise.all([
        devPersistence.createTest(test2),
        devPersistence.createTest(test1)
    ])
        .then(results => {
            devPersistence.createAssessment(new assessmentModel.Assessment('Testuojam Jonuka', results[0].id));
            devPersistence.createAssessment(new assessmentModel.Assessment('Testuojam Petriuka', results[0].id));
            devPersistence.createAssessment(new assessmentModel.Assessment('Testuojam Antaneli', results[1].id));
            devPersistence.createAssessment(new assessmentModel.Assessment('Testuojam Janina', results[1].id));
            return devPersistence.createAssessment(new assessmentModel.Assessment('Testuojam Onute', results[1].id));
        })
        .then(a => {
            const questions = [
                Object.assign(question1, {answer: question1.answers[0].value}),
                Object.assign(question2, {answer: question2.answers[1].value})
            ];
            devPersistence.submitAssessment(a.id, assessmentModel.AssessmentSubmission.submitted(questions, 1));
        })
        .catch(err => log.error(err));
}

new AppServer(devPersistence, process.env.PROFILE, process.env.PORT).start();
