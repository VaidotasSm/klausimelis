class Assessment {
    constructor(name, testId) {
        this.name = name;
        this.testId = testId;
        this.submission = new AssessmentSubmission();
    }
}

class AssessmentSubmission {
    constructor() {
        this.submitted = false;
        this.testVersion = null;
        this.questions = [];
    }

    static submitted(questions, testVersion) {
        const submitted = new AssessmentSubmission();
        submitted.submitted = true;
        submitted.testVersion = testVersion;
        submitted.questions = [...questions];
        return submitted;
    }
}

module.exports = {Assessment, AssessmentSubmission};
