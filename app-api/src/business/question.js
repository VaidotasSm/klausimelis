class TestWrapper {
    constructor(name) {
        this.id = null;
        this.name = name;
        this.current = null;
        this.archive = [];
    }

    setId(id) {
        this.id = id;
        return this;
    }

    setCurrent(test) {
        this.current = test;
        return this;
    }

    addArchive(test) {
        this.archive = [test, ...this.archive];
        return this;
    }
}

class Test {
    constructor(version = 1) {
        this.version = version;
        this.questions = [];
    }

    addQuestion(question) {
        if(!question) {
            throw Error('Question cannot be empty');
        }

        this.questions = [...this.questions, question];
        return this;
    }
}

class Question {
    constructor(name) {
        this.name = name;
        this.answers = [];
    }

    addAnswer(answer) {
        if(!answer) {
            throw Error('Answer cannot be empty');
        }

        this.answers = [...this.answers, answer];
        return this;
    }
}

class Answer {
    constructor(value, correct = false) {
        this.value = value;
        this.correct = correct;
    }
}

module.exports.TestWrapper = TestWrapper;
module.exports.Test = Test;
module.exports.Question = Question;
module.exports.Answer = Answer;
