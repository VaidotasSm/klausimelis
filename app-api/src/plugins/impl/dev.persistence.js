//const Nedb = require('nedb');
const Nedb = require('nedb-promise');

const ENTITY_TEST = 'TEST';
const ENTITY_ASSESSMENT = 'ASSESSMENT';

class DevPersistence {

    constructor() {
        this.db = new Nedb({timestampData: true});
    }

    createTest(test) {
        const entity = Object.assign({}, test, {entityType: ENTITY_TEST});
        return this.db.insert(entity)
            .then(entity => entity ? toDto(entity) : null);
    }

    updateTest(id, {name, current}) {
        return this.findTest(id)
            .then(existing => {
                if (!existing) {
                    throw new Error('Not found');
                }

                const updateQuery = {$set: {}};
                if (name) {
                    updateQuery.$set.name = name;
                }
                if (current) {
                    current.version = existing.current.version + 1;
                    updateQuery.$set.current = current;
                    updateQuery.$push = {archive: existing.current};
                }
                return this.db.update({_id: id}, updateQuery, {multi: false})
                    .then(updateCount => {
                        if (!updateCount) {
                            throw Error('Zero records updated');
                        }
                    });
            });
    }

    findTest(id) {
        return this.db.findOne({_id: id})
            .then(test => test ? toDto(test) : null);
    }

    findTests(pageable) {
        return this.db.cfind({entityType: ENTITY_TEST})
            .sort({updatedAt: -1})
            .exec()
            .then(results => results.map(entity => toDto(entity)));
    }

    createAssessment(assessment) {
        const entity = Object.assign({}, assessment, {entityType: ENTITY_ASSESSMENT});
        return this.db.insert(entity)
            .then(entity => entity ? toDto(entity) : null);
    }

    findAssessments(testId) {
        return this.db.cfind({entityType: ENTITY_ASSESSMENT, testId: testId})
            .sort({updatedAt: -1})
            .exec()
            .then(results => results.map(entity => toDto(entity)));
    }

    findAssessment(id) {
        return this.db.findOne({_id: id})
            .then(entity => entity ? toDto(entity) : null);
    }

    submitAssessment(assessmentId, submission) {
        if(!assessmentId || !submission) {
            throw new Error('Invalid arguments');
        }

        const updateQuery = {$set: {submission: submission}};
        return this.db.update({_id: assessmentId}, updateQuery, {multi: false})
            .then(updateCount => {
                if (!updateCount) {
                    throw Error('Zero records updated');
                }
            });
    }

}

function toDto(entity) {
    entity.id = entity._id;
    delete entity.entityType;
    delete entity._id;
    return entity;
}

function toEntity(dto, entityType) {
    dto._id = dto.id;
    dto.entityType = entityType;
    delete dto.id;
    return dto;
}

module.exports = DevPersistence;
