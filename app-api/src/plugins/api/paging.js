class Pageable {
    constructor(page = 0, pageSize = 20) {
        this.page = Number(page);
        this.pageSize = Number(pageSize);
    }

    nextPage(totalElements) {
        const maxElementsInPage = (this.page + 1) * this.pageSize;
        if (maxElementsInPage < totalElements) {
            return this.page + 1;
        }

        return null;
    }

    previousPage() {
        if (this.page > 0) {
            return this.page - 1;
        }

        return null;
    }

    toPage(content, totalElements = null) {
        return new Page(this, content, totalElements);
    }
}

class Page {
    constructor(pageable, content, totalElements) {
        this.pageable = pageable;
        this.content = content;
        this.totalElements = totalElements || content.length;
    }
}

module.exports.Pageable = Pageable;
module.exports.Page = Page;
