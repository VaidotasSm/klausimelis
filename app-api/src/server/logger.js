const bunyan = require('bunyan');
const log = bunyan.createLogger({name: 'app'});

module.exports = log;
