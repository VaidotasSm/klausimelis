const Page = require('../../plugins/api/paging').Page;
const log = require('../../server/logger');

class HttpUtils {

    static toCollectionResourceWithLinks(page, req) {
        return Object.assign(
            HttpUtils.toCollectionResource(page),
            {_links: HttpUtils.toLinks(page, req)}
        );
    }

    static toCollectionResource(page) {
        if (!page) {
            throw Error('Page cannot be empty');
        }

        return {
            entries: page.content,
            _metadata: {
                totalCount: page.totalElements
            }
        };
    }

    static toLinks(page, req) {
        const nextPage = page.pageable.nextPage(page.totalElements);
        const previousPage = page.pageable.previousPage();

        const _links = {};
        const originalUrl = HttpUtils.getCurrentUrl(req);
        if (nextPage) {
            _links.next = originalUrl.replace(`page=${page.pageable.page}`, `page=${nextPage}`);
        }
        if (previousPage) {
            _links.previous = originalUrl.replace(`page=${page.pageable.page}`, `page=${previousPage}`);
        }
        return _links;
    }

    static getCurrentUrl(req) {
        return `${req.protocol}://${req.headers.host}${req.originalUrl}`;
    }

    static getHostUrl(req) {
        return `${req.protocol}://${req.headers.host}`;
    }

    static getLocationHeader(req, id) {
        return `${HttpUtils.getCurrentUrl(req)}/${id}`;
    }

}

function handleError(err, res) {
    log.error(err);
    if (err instanceof NotFoundException) {
        return res.sendStatus(404);
    }
    if (err instanceof ValidationError) {
        return res.status(400).json(err);
    }
    res.sendStatus(500);
}

class NotFoundException {
}

class ValidationError {
    constructor(field, message) {
        this.field = field;
        this.message = message;
    }
}

module.exports = {HttpUtils, NotFoundException, ValidationError, handleError};
