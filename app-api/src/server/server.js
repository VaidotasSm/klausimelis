const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');

const log = require('./logger');
const rootRouter = require('./routers/root.router');

class AppServer {

    constructor(persistence, profile = 'prod', port = 8080) {
        this.persistence = persistence;
        this.profile = profile;
        this.port = port;
        this.app = express();
        this.handle = null;
    }

    start() {
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({extended: true}));

        if (this.profile === 'dev') {
            this.app.use('/app', express.static(path.join(__dirname, '../../..', 'app-ui')));
        } else {
            this.app.use('/app', express.static(path.join(__dirname, '../../..', 'app-ui/dest')));
        }

        this.app.use('/api', rootRouter.getRouter(this.persistence));
        this.app.get('/', (req, res) => {
            res.redirect('/app/index.html');
        });

        this.handle = this.app.listen(this.port, () => {
            log.info(`Started on port ${this.port}, profile '${this.profile}'`);
        });
    }
}

module.exports = AppServer;
