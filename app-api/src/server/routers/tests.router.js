const express = require('express');
const {HttpUtils, NotFoundException, ValidationError, handleError} = require('../utils/http.utils');
const Pageable = require('../../plugins/api/paging').Pageable;
const AssessmentsRouter = require('./assesments.router');

class TestsRouter {
    constructor(persistence) {
        this.persistence = persistence;
    }

    getRouter() {
        return express.Router()
            .use('/', new AssessmentsRouter(this.persistence).getRouter())
            .get('/:testId', (req, res) => {
                const testId = req.params.testId;
                if (!testId) {
                    return res.sendStatus(404);
                }

                this.persistence.findTest(testId)
                    .then(test => {
                        if (!test) {
                            throw new NotFoundException();
                        }
                        res.status(200).json(test);
                    })
                    .catch(err => handleError(err, res));
            })
            .get('/', (req, res) => {
                const pageable = new Pageable(req.query.page, req.query.pageSize);
                this.persistence.findTests(pageable)
                    .then(tests => {
                        const page = pageable.toPage(tests);
                        const resource = HttpUtils.toCollectionResourceWithLinks(page, req);
                        res.status(200).json(resource);
                    })
                    .catch(err => handleError(err, res));
            })
            .post('/', (req, res) => {
                const testBody = req.body;
                if (!testBody) {
                    return res.status(400).json(new ValidationError('body', 'Cannot be empty'));
                }
                if (!testBody.name) {
                    return res.status(400).json(new ValidationError('name', 'Cannot be empty'));
                }
                if (!testBody.current.questions || testBody.current.questions.length == 0) {
                    return res.status(400).json(new ValidationError('questions', 'Cannot be empty'));
                }
                const questionWithoutName = testBody.current.questions.find(q => !q.name);
                if (questionWithoutName) {
                    return res.status(400).json(new ValidationError('question', 'Cannot be empty'));
                }
                const questionWithoutAnswers = testBody.current.questions.find(q => q.answers.length == 0);
                if (questionWithoutAnswers) {
                    return res.status(400).json(new ValidationError('answers', 'Cannot be empty'));
                }

                testBody.archive = [];
                testBody.current.version = 1;
                this.persistence.createTest(testBody)
                    .then(test => {
                        res.location(HttpUtils.getLocationHeader(req, test.id));
                        res.sendStatus(201);
                    })
                    .catch(err => handleError(err, res));
            })
            .patch('/:testId', (req, res) => {
                const testId = req.params.testId;
                if (!testId) {
                    return res.sendStatus(404);
                }
                const test = req.body;
                if (!test) {
                    return res.sendStatus(400);
                }

                this.persistence.updateTest(testId, req.body)
                    .then(test => res.sendStatus(200))
                    .catch(err => handleError(err, res));
            });
    }
}

module.exports = TestsRouter;
