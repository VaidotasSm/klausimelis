const express = require('express');
const {HttpUtils, NotFoundException, ValidationError, handleError} = require('../utils/http.utils');
const Pageable = require('../../plugins/api/paging').Pageable;
const assessmentModel = require('../../business/assessment');

class AssessmentsRouter {
    constructor(persistence) {
        this.persistence = persistence;
    }

    getRouter() {
        return express.Router()
            .post('/:testId/assessments', (req, res) => {
                const testId = req.params.testId;
                if (!testId) {
                    return res.sendStatus(404);
                }
                if (!req.body) {
                    return res.status(400).json(new ValidationError('body', 'Cannot be empty'));
                }
                const assessment = new assessmentModel.Assessment(req.body.name, testId);
                if (!assessment.name) {
                    return res.status(400).json(new ValidationError('name', 'Cannot be empty'));
                }

                this.persistence.createAssessment(assessment)
                    .then(assessment => {
                        res.location(HttpUtils.getLocationHeader(req, assessment.id));
                        res.sendStatus(201);
                    })
                    .catch(err => handleError(err, res));
            })
            .get('/:testId/assessments/:assessmentId', (req, res) => {
                const testId = req.params.testId;
                const assessmentId = req.params.assessmentId;
                if (!testId || !assessmentId) {
                    return res.sendStatus(404);
                }


                this.persistence.findAssessment(assessmentId)
                    .then(assessment => {
                        if (!assessment || assessment.testId !== testId) {
                            throw new NotFoundException();
                        }
                        res.status(200).json(withAssessmentLinks(req, assessment));
                    })
                    .catch(err => handleError(err, res));
            })
            .get('/:testId/assessments', (req, res) => {
                const testId = req.params.testId;
                if (!testId) {
                    return res.sendStatus(404);
                }

                const pageable = new Pageable(req.query.page, req.query.pageSize);
                this.persistence.findAssessments(testId)
                    .then(assessments => {
                        const page = pageable.toPage(assessments.map(a => withAssessmentLinks(req, a)));
                        const resource = HttpUtils.toCollectionResourceWithLinks(page, req);
                        res.status(200).json(resource);
                    })
                    .catch(err => handleError(err, res));
            });
    }
}

function withAssessmentLinks(req, assessment) {
    const participateLink = `${HttpUtils.getHostUrl(req)}/api/public/assessments/${assessment.testId}/${assessment.id}`;
    assessment._links = {
        participate: participateLink
    };
    return assessment;
}

module.exports = AssessmentsRouter;
