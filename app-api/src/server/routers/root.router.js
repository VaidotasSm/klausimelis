const express = require('express');
const log = require('../logger');
const TestsRouter = require('./tests.router');
const AssessmentsPublicRouter = require('./assesments-public.router');

module.exports.getRouter = function (testsPersistence) {
    return express.Router()
        .use(function (req, res, next) {
            log.info(`${req.url} - ${req.method}`);
            next();
        })
        .use('/tests', new TestsRouter(testsPersistence).getRouter())
        .use('/public/assessments', new AssessmentsPublicRouter(testsPersistence).getRouter());
};
