const express = require('express');
const log = require('../../server/logger');
const {HttpUtils, NotFoundException, handleError} = require('../utils/http.utils');
const {Assessment, AssessmentSubmission} = require('../../business/assessment');

class AssessmentsPublicRouter {
    constructor(persistence) {
        this.persistence = persistence;
    }

    getRouter() {
        return express.Router()
            .get('/:testId/:assessmentId', (req, res) => {
                const testId = req.params.testId;
                const assessmentId = req.params.assessmentId;
                if (!testId || !assessmentId) {
                    return res.sendStatus(404);
                }

                let assessment = null;
                this.persistence.findAssessment(assessmentId)
                    .then(a => {
                        if (!a || a.testId !== testId) {
                            throw new NotFoundException();
                        }
                        assessment = a;
                    })
                    .then(() => this.persistence.findTest(testId))
                    .then(test => {
                        if (!test) {
                            throw new NotFoundException();
                        }

                        test.current.questions.forEach(q => {
                            q.answers.forEach(a => delete a.correct);
                        });
                        assessment._embedded = {
                            test: {
                                name: test.name,
                                content: test.current
                            }
                        };
                        res.status(200).json(assessment);
                    })
                    .catch(err => handleError(err, res));
            })
            .patch('/:testId/:assessmentId', (req, res) => {
                const testId = req.params.testId;
                const assessmentId = req.params.assessmentId;
                if (!testId || !assessmentId) {
                    return res.sendStatus(404);
                }
                if (!req.body || !req.body.submission || !req.body.submission.questions || !req.body.submission.questions.length) {
                    log.error(req.body);
                    return res.sendStatus(400);
                }

                this.persistence.findAssessment(assessmentId)
                    .then(a => {
                        if (!a || a.testId !== testId || a.submission.submitted) {
                            throw new NotFoundException();
                        }
                    })
                    .then(() => this.persistence.findTest(testId))
                    .then(test => AssessmentSubmission.submitted(req.body.submission.questions, test.current.version))
                    .then(submission => this.persistence.submitAssessment(assessmentId, submission))
                    .then(() => res.sendStatus(200))
                    .catch(err => handleError(err, res));
            });
    }
}

module.exports = AssessmentsPublicRouter;
