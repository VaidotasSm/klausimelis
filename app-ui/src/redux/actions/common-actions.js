const CommonActions ={
    COMMON_DATA_LOAD_STARTED: 'COMMON_DATA_LOAD_STARTED'
};

class ActionUtils {
    static createAction(type, payload) {
        return {type, payload};
    }
}

export {CommonActions, ActionUtils};
