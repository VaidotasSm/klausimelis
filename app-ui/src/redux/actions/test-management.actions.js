import {CALL_API} from "redux-api-middleware";
import {CommonActions, ActionUtils} from "./common-actions.js";

class TestManagementActions {

    static getTests() {
        return {
            [CALL_API]: {
                endpoint: '/api/tests',
                method: 'GET',
                types: [
                    CommonActions.COMMON_DATA_LOAD_STARTED,
                    TestManagementActions.TESTS_GET_ALL_SUCCESS,
                    TestManagementActions.TESTS_GET_ALL_FAILURE
                ]
            }
        };
    }

    static getTest(testId) {
        return {
            [CALL_API]: {
                endpoint: `/api/tests/${testId}`,
                method: 'GET',
                types: [
                    CommonActions.COMMON_DATA_LOAD_STARTED,
                    TestManagementActions.TESTS_GET_ONE_SUCCESS,
                    TestManagementActions.TESTS_GET_ONE_FAILURE
                ]
            }
        };
    }

    static createTest(test) {
        return {
            [CALL_API]: {
                endpoint: `/api/tests`,
                method: 'POST',
                body: JSON.stringify(test),
                headers: {'Content-Type': 'application/json'},
                types: [
                    CommonActions.COMMON_DATA_LOAD_STARTED,
                    TestManagementActions.TESTS_CREATE_SUCCESS,
                    TestManagementActions.TESTS_CREATE_FAILURE
                ]
            }
        };
    }

    static updateTest(testId, testUpdate) {
        if (!testId) {
            throw new Error('test id cannot be empty');
        }

        return {
            [CALL_API]: {
                endpoint: `/api/tests/${testId}`,
                method: 'PATCH',
                body: JSON.stringify(testUpdate),
                headers: {'Content-Type': 'application/json'},
                types: [
                    CommonActions.COMMON_DATA_LOAD_STARTED,
                    TestManagementActions.TESTS_UPDATE_SUCCESS,
                    TestManagementActions.TESTS_UPDATE_FAILURE
                ]
            }
        };
    }

    static getTestAssessments(testId) {
        return {
            [CALL_API]: {
                endpoint: `/api/tests/${testId}/assessments`,
                method: 'GET',
                types: [
                    CommonActions.COMMON_DATA_LOAD_STARTED,
                    TestManagementActions.TEST_ASSESSMENTS_GET_ALL_SUCCESS,
                    TestManagementActions.TEST_ASSESSMENTS_GET_ALL_FAILURE
                ]
            }
        };
    }

    static getAssessment(testId, assessmentId) {
        return {
            [CALL_API]: {
                endpoint: `/api/tests/${testId}/assessments/${assessmentId}`,
                method: 'GET',
                types: [
                    CommonActions.COMMON_DATA_LOAD_STARTED,
                    TestManagementActions.TEST_ASSESSMENTS_GET_ONE_SUCCESS,
                    TestManagementActions.TEST_ASSESSMENTS_GET_ONE_FAILURE
                ]
            }
        };
    }

    static submitAssessment(testId, assessmentId, questionsWithAnswers) {
        const body = {submission: {questions: questionsWithAnswers}};
        return {
            [CALL_API]: {
                endpoint: `/api/public/assessments/${testId}/${assessmentId}`,
                method: 'PATCH',
                body: JSON.stringify(body),
                headers: {'Content-Type': 'application/json'},
                types: [
                    CommonActions.COMMON_DATA_LOAD_STARTED,
                    TestManagementActions.TEST_ASSESSMENTS_SUBMIT_SUCCESS,
                    TestManagementActions.TEST_ASSESSMENTS_SUBMIT_FAILURE
                ]
            }
        };
    }

    static createTestAssessment(assessment) {
        return {
            [CALL_API]: {
                endpoint: `/api/tests/${assessment.testId}/assessments`,
                method: 'POST',
                body: JSON.stringify(assessment),
                headers: {'Content-Type': 'application/json'},
                types: [
                    CommonActions.COMMON_DATA_LOAD_STARTED,
                    TestManagementActions.TEST_ASSESSMENTS_CREATE_SUCCESS,
                    TestManagementActions.TEST_ASSESSMENTS_CREATE_FAILURE
                ]
            }
        };
    }

    static selectAssessment(assessment) {
        return ActionUtils.createAction(TestManagementActions.TEST_ASSESSMENTS_ONE_SELECTED, assessment);
    }

    static unselectAssessment() {
        return ActionUtils.createAction(TestManagementActions.TEST_ASSESSMENTS_ONE_UNSELECTED, null);
    }

}

TestManagementActions.TESTS_GET_ALL_SUCCESS = 'TESTS_GET_ALL_SUCCESS';
TestManagementActions.TESTS_GET_ALL_FAILURE = 'TESTS_GET_ALL_FAILURE';
TestManagementActions.TESTS_GET_ONE_SUCCESS = 'TESTS_GET_ONE_SUCCESS';
TestManagementActions.TESTS_GET_ONE_FAILURE = 'TESTS_GET_ONE_FAILURE';
TestManagementActions.TESTS_CREATE_SUCCESS = 'TESTS_CREATE_SUCCESS';
TestManagementActions.TESTS_CREATE_FAILURE = 'TESTS_CREATE_FAILURE';
TestManagementActions.TESTS_UPDATE_SUCCESS = 'TESTS_UPDATE_SUCCESS';
TestManagementActions.TESTS_UPDATE_FAILURE = 'TESTS_UPDATE_FAILURE';
TestManagementActions.TEST_ASSESSMENTS_GET_ALL_SUCCESS = 'TEST_ASSESSMENTS_GET_ALL_SUCCESS';
TestManagementActions.TEST_ASSESSMENTS_GET_ALL_FAILURE = 'TEST_ASSESSMENTS_GET_ALL_FAILURE';
TestManagementActions.TEST_ASSESSMENTS_GET_ONE_SUCCESS = 'TEST_ASSESSMENTS_GET_ONE_SUCCESS';
TestManagementActions.TEST_ASSESSMENTS_GET_ONE_FAILURE = 'TEST_ASSESSMENTS_GET_ONE_FAILURE';
TestManagementActions.TEST_ASSESSMENTS_CREATE_SUCCESS = 'TEST_ASSESSMENTS_CREATE_SUCCESS';
TestManagementActions.TEST_ASSESSMENTS_CREATE_FAILURE = 'TEST_ASSESSMENTS_CREATE_FAILURE';
TestManagementActions.TEST_ASSESSMENTS_SUBMIT_SUCCESS = 'TEST_ASSESSMENTS_SUBMIT_SUCCESS';
TestManagementActions.TEST_ASSESSMENTS_SUBMIT_FAILURE = 'TEST_ASSESSMENTS_SUBMIT_FAILURE';
TestManagementActions.TEST_ASSESSMENTS_ONE_SELECTED = 'TEST_ASSESSMENTS_ONE_SELECTED';
TestManagementActions.TEST_ASSESSMENTS_ONE_UNSELECTED = 'TEST_ASSESSMENTS_ONE_UNSELECTED';

export {TestManagementActions};
