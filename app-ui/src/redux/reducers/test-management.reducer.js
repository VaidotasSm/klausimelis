import toastr from "toastr";
import {hashHistory} from "react-router";
import {TestManagementActions} from "../actions/test-management.actions";

const initialState = {tests: [], test: null, testAssessments: [], selectedAssessment: null};

function testManagementReducer(state = {tests: [], test: null, testAssessments: [], selectedAssessment: null}, action) {
    switch (action.type) {
        case TestManagementActions.TESTS_GET_ALL_SUCCESS:
            return cloneWith(state, {tests: action.payload.entries});
        case TestManagementActions.TESTS_GET_ALL_FAILURE:
            toastr.error('Could not load tests');
            return state;
        case TestManagementActions.TESTS_GET_ONE_SUCCESS:
            return cloneWith(state, {test: action.payload});
            return state;
        case TestManagementActions.TESTS_GET_ONE_FAILURE:
            toastr.error('Could not load test');
            return state;
        case TestManagementActions.TEST_ASSESSMENTS_GET_ALL_SUCCESS:
            return cloneWith(state, {testAssessments: action.payload.entries});
        case TestManagementActions.TEST_ASSESSMENTS_GET_ALL_FAILURE:
            toastr.error('Could not load assessments');
            return state;
        case TestManagementActions.TEST_ASSESSMENTS_GET_ONE_SUCCESS:
            return cloneWith(state, {selectedAssessment: action.payload});
        case TestManagementActions.TEST_ASSESSMENTS_GET_ONE_FAILURE:
            toastr.error('Could not load assessment');
            return state;
        case TestManagementActions.TEST_ASSESSMENTS_CREATE_SUCCESS:
            const path = `/private/tests/${state.test.id}/view`;
            hashHistory.push(path);
            toastr.success('Successfully created assessment');
            return state;
        case TestManagementActions.TEST_ASSESSMENTS_CREATE_FAILURE:
            toastr.error(parseErrorText(action), 'Could not create assessment');
            return state;
        case TestManagementActions.TEST_ASSESSMENTS_SUBMIT_SUCCESS:
            hashHistory.push('/public/assessments/done');
            toastr.success('Successfully submitted');
            return cloneWith(state, {test: null, selectedAssessment: null});
        case TestManagementActions.TEST_ASSESSMENTS_SUBMIT_FAILURE:
            toastr.error('Could not submit assessment');
            return state;
        case TestManagementActions.TESTS_CREATE_SUCCESS:
            hashHistory.push('/private/tests');
            toastr.success('New test created');
            return state;
        case TestManagementActions.TESTS_CREATE_FAILURE:
            toastr.error(parseErrorText(action), 'Could not create test');
            return state;
        case TestManagementActions.TESTS_UPDATE_SUCCESS:
            hashHistory.push('/private/tests');
            toastr.success('Test was successfully updated');
            return cloneWith(state, initialState);
        case TestManagementActions.TESTS_UPDATE_FAILURE:
            toastr.error('Could not update test');
            return state;
        case TestManagementActions.TEST_ASSESSMENTS_ONE_SELECTED:
            return cloneWith(state, {selectedAssessment: action.payload});
        case TestManagementActions.TEST_ASSESSMENTS_ONE_UNSELECTED:
            return cloneWith(state, {selectedAssessment: null});
        default:
            return state;
    }
}

function cloneWith(state, overrideObject) {
    return Object.assign({}, state, overrideObject);
}

function parseErrorText(action) {
    if (!action.payload.status || !action.payload.response) {
        return 'unknown';
    }

    if (action.payload.status === 400) {
        return `${action.payload.response.field}: ${action.payload.response.message}`;
    }
}

export {testManagementReducer};
