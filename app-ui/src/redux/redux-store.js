import {createStore, applyMiddleware} from "redux";
import {rootReducer} from "./root.reducer.js";
import {apiMiddleware} from "redux-api-middleware";
import thunk from "redux-thunk";

function configureStore(initialState = {}) {
    return createStore(
        rootReducer,
        initialState,
        applyMiddleware(apiMiddleware, thunk)
    );
}

export {configureStore};
