import {combineReducers} from "redux";
import {testManagementReducer} from "./reducers/test-management.reducer.js";

const rootReducer = combineReducers({
    testManagementReducer: testManagementReducer
});
export {rootReducer};

