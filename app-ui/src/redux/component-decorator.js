import {connect} from "react-redux";

/**
 * Redux-ify component. Only for single store components.
 */
export function decorateComponent(component, reducerName) {
    function mapStateToProps(state, ownProps) {
        return {reducer: state[reducerName]};
    }

    return connect(mapStateToProps)(component)
}
