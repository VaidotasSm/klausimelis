const CommonStyles = {
    pointerCursorStyle: {cursor: 'pointer'},
    mainWidthClass: 'col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2',
    displayInlineStyle: {display: "inline"},
    correctAnswerStyle: {color: "green", fontWeight: "bold"},
    selectedIncorrectAnswerStyle: {color: "red", fontWeight: "bold", fontSize: "150%"},
    selectedCorrectAnswerStyle: {color: "green", fontWeight: "bold", fontSize: "150%"}
};

export {CommonStyles};

