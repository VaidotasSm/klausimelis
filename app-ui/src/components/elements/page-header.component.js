import React from "react";

function PageHeader({text}) {
    return (
        <div className="row">
            <div className="text-center col-xs-8 col-xs-offset-2 col-sm-4 col-sm-offset-4">
                <h3>{text}</h3>
            </div>
        </div>
    );
}
PageHeader.propTypes = {
    text: React.PropTypes.string.isRequired
};

export {PageHeader};
