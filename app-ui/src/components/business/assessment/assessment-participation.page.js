import React from "react";
import {hashHistory} from "react-router";
import {decorateComponent} from "../../../redux/component-decorator";
import {TestManagementActions} from "../../../redux/actions/test-management.actions";

class AssessmentParticipationPage extends React.Component {

    constructor(props, context) {
        super(props, context);

        const testId = this.props.params.testId;
        const assessmentId = this.props.params.assessmentId;
        this.state = {testId, assessmentId};

        this.props.dispatch(TestManagementActions.getTest(testId));
        this.props.dispatch(TestManagementActions.getAssessment(testId, assessmentId));

        this.onAnswerChange = this.onAnswerChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    canBeSubmitted(test, assessment) {
        if (test.id !== assessment.testId) {
            return false;
        }
        if (assessment.submission && assessment.submission.submitted) {
            return false;
        }
        return true;
    }

    componentWillReceiveProps(props) {
        const test = props.reducer.test;
        const assessment = props.reducer.selectedAssessment;

        const stateUpdates = {test, assessment};
        this.setState(Object.assign({}, this.state, stateUpdates));
        if (test && assessment && !this.canBeSubmitted(test, assessment)) {
            hashHistory.push('/not-found');
        }
    }

    onAnswerChange(event) {
        const test = this.props.reducer.test;
        test.current.questions[event.target.name].answer = event.target.value
    }

    onSubmit(event) {
        event.preventDefault();
        const test = this.props.reducer.test;
        this.props.dispatch(TestManagementActions.submitAssessment(
            this.state.testId,
            this.state.assessmentId,
            test.current.questions
        ));
    }

    render() {
        const test = this.props.reducer.test;
        const assessment = this.props.reducer.selectedAssessment;
        if (!test || !assessment) {
            return <div className="text-center"><h3>Loading...</h3></div>
        }

        const questions = test.current.questions;
        questions.forEach(q => q.answer = null);
        const questionStyle = {paddingLeft: "20px"};
        const answerStyle = {paddingLeft: "40px", paddingTop: "5px"};
        return (
            <div>
                <div className="row">
                    <div className="col-xs-4"><label>Test Name</label></div>
                    <div className="col-xs-8"><label>{test.name}</label></div>
                </div>

                <form onSubmit={this.onSubmit} method="post">
                    <div className="list-group">
                        {questions.map((question, questionIndex) =>
                            <div key={questionIndex} className="list-group-item">
                                <div className="row" style={questionStyle}>
                                    <strong>{question.name}</strong>
                                </div>
                                {question.answers.map((answer, j) =>
                                    <div key={j} className="row" style={answerStyle}>
                                        <input type="radio" name={questionIndex} value={answer.value} onChange={this.onAnswerChange}/>{answer.value}
                                    </div>
                                )}
                            </div>
                        )}
                    </div>
                    <div className="row">
                        <div className="pull-right">
                            <input type="submit" className="btn btn-default" value="Submit"/>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

const decoratedComponent = decorateComponent(AssessmentParticipationPage, 'testManagementReducer');
export {decoratedComponent as AssessmentParticipationPage};
