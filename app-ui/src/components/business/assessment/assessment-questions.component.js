import React from "react";

function QuestionsOverview({questions}) {
    return (
        <div className="list-group">
            {questions.map((question, i) =>
                <div key={i}>
                    <QuestionOverview question={question}/>
                </div>
            )}
        </div>
    );
}

QuestionsOverview.propTypes = {
    questions: React.PropTypes.array.isRequired
};
export {QuestionsOverview};

