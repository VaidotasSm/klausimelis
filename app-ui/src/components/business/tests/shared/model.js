class AssessmentModel {
    constructor(name, testId) {
        this.name = name;
        this.testId = testId;
        this.submission = {};
    }
}

class TestModel {
    constructor(name = '') {
        this.name = name;
        this.current = {
            questions: []
        };
    }

    setQuestions(questions = []) {
        this.current.questions = questions;
        return this;
    }
}

class QuestionModel {
    constructor(name = '') {
        this.name = name;
        this.answers = [];
    }
}

class AnswerModel {
    constructor(value, editMode = false, correct = false) {
        this.value = value;
        this.editMode = editMode;
        this.correct = correct;
    }
}

export {AssessmentModel, TestModel, QuestionModel, AnswerModel};
