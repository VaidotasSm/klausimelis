import React from "react";
import {hashHistory} from "react-router";
import {decorateComponent} from "../../../redux/component-decorator.js";
import {TestManagementActions} from "../../../redux/actions/test-management.actions.js";
import {PageHeader} from "../../elements/page-header.component.js";
import {QuestionsOverview} from "./elements/questions-overview.component";
import {AssessmentsOverview} from "./elements/assessments-overview.component";
import {AssessmentView} from "./elements/assessment-view.component";
import {TestEdit} from "./elements/test-edit.component";
import {TestModel} from "./shared/model";

class TestViewPage extends React.Component {
    constructor(props, context) {
        super(props, context);

        const testId = this.props.params.testId;
        this.state = {testId, test: null, update: null};
        this.props.dispatch(TestManagementActions.getTest(testId));
        this.props.dispatch(TestManagementActions.unselectAssessment());
        this.props.dispatch(TestManagementActions.getTestAssessments(testId));

        this.onAssessmentUnselected = this.onAssessmentUnselected.bind(this);
        this.onCreateAssessmentClick = this.onCreateAssessmentClick.bind(this);
        this.onEditTestClick = this.onEditTestClick.bind(this);
        this.onEditCancelClick = this.onEditCancelClick.bind(this);
        this.onEditSaveClick = this.onEditSaveClick.bind(this);
    }

    componentWillReceiveProps(props) {
        if (!this.state.test || this.state.test.id !== this.state.testId) {
            this.setState(Object.assign({}, this.state, {test: props.reducer.test}));
        }
    }

    onEditTestClick() {
        const update = new TestModel(this.state.test.name)
            .setQuestions(this.state.test.current.questions.map(q => Object.assign({}, q)));
        this.setState(Object.assign({}, this.state, {update}));
    }

    onEditCancelClick() {
        this.setState(Object.assign({}, this.state, {update: null}));
    }

    onEditSaveClick() {
        this.props.dispatch(TestManagementActions.updateTest(this.state.testId, this.state.update));
    }

    onAssessmentUnselected() {
        this.props.dispatch(TestManagementActions.unselectAssessment());
        this.setState(Object.assign({}, this.state));
    }

    onCreateAssessmentClick() {
        hashHistory.push(`/private/tests/${this.state.testId}/assessments/create`);
    }

    render() {
        const test = this.state.test;
        const currentVersion = test ? test.current.version : 0;
        const questions = test ? test.current.questions : [];

        const assessmentView = this.props.reducer.selectedAssessment ?
            <div>
                <div className="row">
                    <div className="col-xs-12">
                        <div className="pull-right">
                            <button className="btn btn-default btn-xs" onClick={this.onAssessmentUnselected}>Back
                            </button>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-12">
                        <AssessmentView assessment={this.props.reducer.selectedAssessment} test={test}/>
                    </div>
                </div>
            </div>
            :
            <AssessmentsOverview assessments={this.props.reducer.testAssessments}/>;
        const testView =
            <div>
                <div className="row">
                    <div className="col-xs-12">
                        <div className="form-inline pull-right">
                            <div className="input-group">
                        <span className="input-group-btn">
                            <button className="btn btn-xs btn-default form-control"
                                    onClick={this.onCreateAssessmentClick}>
                                Create Assessment
                            </button>
                        </span>
                                <span className="input-group-btn">
                            <button className="btn btn-xs btn-default form-control"
                                    onClick={this.onEditTestClick}>Edit</button>
                        </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-12">
                        <PageHeader text={test ? test.name : 'Test not Found'}/>
                    </div>
                </div>
                <hr />

                <div className="row">
                    <div className="col-xs-12">
                        <h4>Questions (version {currentVersion})</h4>
                    </div>
                </div>
                <hr />
                <div className="row">
                    <div className="col-xs-12">
                        <QuestionsOverview questions={questions}/>
                    </div>
                </div>

                <div>
                    <div className="row">
                        <div className="col-xs-12"><h4>Assessments</h4></div>
                    </div>
                    <hr />
                    <div className="row">
                        <div className="col-xs-12">{assessmentView}</div>
                    </div>
                </div>
            </div>;

        const testEditView = this.state.update ?
            <div>
                <PageHeader text="Update Test"/>
                <hr />
                <TestEdit test={this.state.update}/>

                <div className="row">
                    <div className="col-xs-12">
                        <div className="pull-right">
                            <button className="btn btn-default" onClick={this.onEditSaveClick}>Save</button>
                            <button className="btn btn-default" onClick={this.onEditCancelClick}>Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
            :
            <div></div>;

        return (
            <div>
                {this.state.update ?
                    testEditView :
                    testView
                }
            </div>
        );
    }
}

const decoratedComponent = decorateComponent(TestViewPage, 'testManagementReducer');
export {decoratedComponent as TestViewPage};
