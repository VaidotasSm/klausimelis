import React from "react";
import {AnswerModel} from "../shared/model";
import {AnswerEdit} from "./answer-edit.component";

class QuestionEdit extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {};
        this.onNameChange = this.onNameChange.bind(this);
        this.onAddAnswer = this.onAddAnswer.bind(this);
        this.onInternalChanges = this.onInternalChanges.bind(this);
    }

    onNameChange(event) {
        this.props.question.name = event.target.value;
        this.setState(Object.assign(this.state));
    }

    onAddAnswer() {
        const new1 = new AnswerModel('', true);
        this.props.question.answers = [...this.props.question.answers, new1];
        this.setState(this.state);
    }

    onInternalChanges() {
        this.setState(this.state);
    }

    render() {
        const displayOptionalButtons = this.props.onSave || this.props.onCancel;
        const mainWidth = displayOptionalButtons ? 'col-xs-10' : 'col-xs-12';
        const optionalButtonsView = displayOptionalButtons ?
            <div className="col-xs-2">
                {this.props.onSave ?
                    <button className="btn btn-default btn-xs"
                            onClick={() => this.props.onSave(this.props.question)}>
                        Save
                    </button> :
                    <span></span>
                }

                {this.props.onCancel ?
                    <button className="btn btn-default btn-xs" onClick={this.props.onCancel}>Cancel</button>
                    :
                    <span></span>
                }
            </div>
            :
            <div></div>;

        return (
            <div className="row">
                <div className={mainWidth}>
                    <div>
                        <div className="list-group-item">
                            <input type="text" value={this.props.question.name} onChange={this.onNameChange}/>
                        </div>
                        <div className="list-group-item">
                            <div className="row">
                                <div className="col-xs-6 col-sm-3">
                                    <label>Answers</label>
                                    <button className="btn btn-default btn-xs" onClick={this.onAddAnswer}>+</button>
                                </div>
                                <div className="col-xs-6 col-sm-9 list-group">
                                    {this.props.question.answers.map((answer, i) =>
                                        <div key={`${i}-${answer.name}`}>
                                            <AnswerEdit answer={answer} onChanges={this.onInternalChanges}/>
                                        </div>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {optionalButtonsView}
            </div>
        );
    }
}

QuestionEdit.propTypes = {
    question: React.PropTypes.object.isRequired,
    onSave: React.PropTypes.func,
    onCancel: React.PropTypes.func
};
export {QuestionEdit};
