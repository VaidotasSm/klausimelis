import React from "react";
import {QuestionOverview} from "./question-overview.component";
import {QuestionEdit} from "./question-edit.component";

class QuestionEditOverview extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {question: this.props.question, editMode: this.props.editMode};
        this.onEditMode = this.onEditMode.bind(this);
        this.onCancel = this.onCancel.bind(this);
        this.onSave = this.onSave.bind(this);
    }

    onEditMode() {
        this.setState(Object.assign({}, this.state, {editMode: true}));
    }

    onCancel() {
        this.setState(Object.assign({}, this.state, {editMode: false}));
    }

    onSave(question) {
        const updatedQuestion = Object.assign(this.state.question, question);
        this.setState(Object.assign({}, this.state, {question: updatedQuestion, editMode: false}));
    }

    render() {
        const view = this.state.editMode ?
            <div className="list-group-item">
                <QuestionEdit question={this.state.question} onSave={this.onSave} onCancel={this.onCancel}/>
            </div>
            :
            <div className="row list-group-item">
                <div className="col-xs-10">
                    <QuestionOverview question={this.state.question} alwaysExpanded={true}/>
                </div>
                <div className="col-xs-2">
                    <button className="btn btn-default btn-xs" onClick={this.onEditMode}>Edit</button>
                </div>
            </div>;

        return (
            <div>
                {view}
            </div>
        );
    }
}

QuestionEditOverview.propTypes = {
    question: React.PropTypes.object.isRequired,
    editMode: React.PropTypes.bool
};
export {QuestionEditOverview};
