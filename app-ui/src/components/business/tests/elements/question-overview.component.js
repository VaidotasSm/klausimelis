import React from "react";
import {CommonStyles} from "../../../shared/common-styles";

class QuestionOverview extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {expandAnswers: this.props.alwaysExpanded};
        this.onQuestionSelected = this.onQuestionSelected.bind(this);
    }

    onQuestionSelected() {
        if(!this.props.alwaysExpanded) {
            let expandAnswers = !this.state.expandAnswers;
            this.setState(Object.assign({}, this.state, {expandAnswers}));
        }
    }

    getAnswerStyle(answer) {
        return answer.correct ? CommonStyles.correctAnswerStyle : {};
    }

    render() {
        const question = this.props.question;

        return (
            <div>
                <div className="list-group-item"
                     style={CommonStyles.pointerCursorStyle}
                     onClick={this.onQuestionSelected}>
                    {question.name}
                </div>
                {this.state.expandAnswers ?
                    <div className="list-group-item">
                        <div className="row">
                            <div className="col-xs-6 col-sm-3">
                                <label>Answers</label>
                            </div>
                            <div className="col-xs-6 col-sm-9 list-group">
                                {question.answers.map((answer, i) =>
                                    <div key={i} className="row">
                                        <div>
                                            <span style={this.getAnswerStyle(answer)}>
                                                {answer.value}
                                            </span>
                                        </div>
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>
                    :
                    <div></div>
                }
            </div>
        );
    }
}

QuestionOverview.propTypes = {
    question: React.PropTypes.object.isRequired,
    alwaysExpanded: React.PropTypes.bool
};
export {QuestionOverview};
