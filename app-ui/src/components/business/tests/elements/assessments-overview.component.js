import React from "react";
import {AssessmentOverview} from "./assessment-overview.component.js";

function AssessmentsOverview({assessments}) {
    return (
        <div className="list-group">
            {assessments.map(assessment =>
                <div key={assessment.id} className="list-group-item row">
                    <AssessmentOverview assessment={assessment}/>
                </div>
            )}
        </div>
    );
}

AssessmentsOverview.propTypes = {
    assessments: React.PropTypes.array.isRequired
};
export {AssessmentsOverview};

