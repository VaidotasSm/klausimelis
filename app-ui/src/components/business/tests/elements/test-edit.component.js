import React from "react";
import {QuestionModel} from "../shared/model";
import {QuestionEdit} from "./question-edit.component";

class TestEdit extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.state = {test: this.props.test};
        this.onQuestionAdd = this.onQuestionAdd.bind(this);
        this.onNameChange = this.onNameChange.bind(this);
    }

    onNameChange(event) {
        this.state.test.name = event.target.value;
        this.setState(this.state);
    }

    onQuestionAdd() {
        this.state.test.current.questions.push(new QuestionModel());
        this.setState(this.state);
    }

    render() {
        const questionHeaderStyle = {paddingTop: "40px", paddingBottom: "20px"};
        const noQuestionsView = !this.state.test.current.questions.length ?
            <div className="list-group-item">No Questions</div> :
            <div></div>;

        return (
            <div>
                <div className="row form-group">
                    <div className="col-xs-4"><label>Name</label></div>
                    <div className="col-xs-8">
                        <input type="text" className="form-control" name="name" value={this.state.test.name}
                               onChange={this.onNameChange}/>
                    </div>
                </div>
                <div className="row" style={questionHeaderStyle}>
                    <div className="col-xs-4"></div>
                    <div className="col-xs-4"><h4 className="text-center">Questions</h4></div>
                    <div className="col-xs-4">
                        <div className="pull-right">
                            <button className="btn btn-default btn-xs" onClick={this.onQuestionAdd}>+</button>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="list-group">
                        {this.state.test.current.questions.map((question, i) =>
                            <div key={i}>
                                <QuestionEdit question={question}/>
                            </div>
                        )}
                        {noQuestionsView}
                    </div>
                </div>

            </div>
        );
    }
}

TestEdit.propTypes = {
    test: React.PropTypes.object.isRequired
};
export {TestEdit};
