import React from "react";
import {CommonStyles} from "../../../shared/common-styles";

class AnswerEdit extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {};

        this.onAnswerChange = this.onAnswerChange.bind(this);
        this.onSaveClick = this.onSaveClick.bind(this);
        this.onEditClick = this.onEditClick.bind(this);
        this.onCorrectChanged = this.onCorrectChanged.bind(this);
    }

    componentWillReceiveProps(props) {
        this.setState(this.state);
    }

    onAnswerChange(event) {
        this.props.answer.value = event.target.value;
        this.setState(this.state);
    }

    onSaveClick() {
        this.props.answer.editMode = false;
        this.setState(this.state);
        this.props.onChanges();
    }

    onCorrectChanged(event) {
        this.props.answer.correct = event.target.checked;
        this.setState(this.state);
    }

    onEditClick() {
        this.props.answer.editMode = true;
        this.setState(this.state);
    }

    render() {
        const view = this.props.answer.editMode ?
            <div className="row form-inline">
                <div className="col-xs-8 form-group">
                    <input type="text" className="form-control"
                           value={this.props.answer.value}
                           onChange={this.onAnswerChange}/>
                    <input type="checkbox" className="form-control"
                           value={this.props.answer.correct}
                           checked={!!this.props.answer.correct}
                           onChange={this.onCorrectChanged}/>
                    Correct
                </div>
                <div className="col-xs-4">
                    <div className="pull-right">
                        <button className="btn btn-default btn-xs form-control" onClick={this.onSaveClick}>Save</button>
                    </div>
                </div>
            </div>
            :
            <div className="row">
                <div className="col-xs-8">
                <span style={this.props.answer.correct ? CommonStyles.correctAnswerStyle : {}}>
                    {this.props.answer.value}
                </span>
                </div>
                <div className="col-xs-4">
                    <button className="btn btn-default btn-xs" onClick={this.onEditClick}>Edit</button>
                </div>
            </div>;

        return (
            <div className="row">
                {view}
            </div>
        );
    }

}

AnswerEdit.propTypes = {
    answer: React.PropTypes.object.isRequired,
    onChanges: React.PropTypes.func.isRequired
};
export {AnswerEdit};