import React from "react";
import {TestManagementActions} from "../../../../redux/actions/test-management.actions";
import {decorateComponent} from "../../../../redux/component-decorator";
import {CommonStyles} from "../../../shared/common-styles";

class AssessmentOverview extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.state = {assessmentUrl: null};
        this.onAssessmentViewClick = this.onAssessmentViewClick.bind(this);
        this.onAssessmentClick = this.onAssessmentClick.bind(this);
    }

    onAssessmentViewClick() {
        this.props.dispatch(TestManagementActions.selectAssessment(this.props.assessment));
    }

    onAssessmentClick() {
        let updatedObj = {};
        if (this.state.assessmentUrl || this.props.assessment.submission.submitted) {
            updatedObj.assessmentUrl = null;
        } else {
            const baseUrl = `${window.location.protocol}//${window.location.host}`;
            updatedObj.assessmentUrl = `${baseUrl}/app/index.html#/public/assessments/${this.props.assessment.testId}/${this.props.assessment.id}`;
        }

        this.setState(Object.assign({}, this.state, updatedObj));
    }

    render() {
        const assessmentUrlContainerStyle = {paddingTop: "20px"};
        const inputWidthStyle = {width: "100%"};
        const assessmentUrlView = this.state.assessmentUrl ?
            <div className="row" style={assessmentUrlContainerStyle}>
                <div className="col-xs-3">
                    Share link
                </div>
                <div className="col-xs-9">
                    <input type="text" value={this.state.assessmentUrl} readOnly="true" style={inputWidthStyle}/>
                </div>
            </div>
            :
            <div className="row"></div>;

        return (
            <div>
                <div className="row">
                    <div className="col-xs-8" style={CommonStyles.pointerCursorStyle} onClick={this.onAssessmentClick}>
                        <span>{this.props.assessment.name}</span>
                    </div>
                    <div className="col-xs-4">
                        <div className="pull-right">
                            {this.props.assessment.submission.submitted ?
                                <button className="btn btn-default btn-xs" onClick={this.onAssessmentViewClick}>
                                    View Answers
                                </button>
                                :
                                <span>Not yet submitted</span>
                            }
                        </div>
                    </div>
                </div>
                {assessmentUrlView}
            </div>
        );
    }
}

AssessmentOverview.propTypes = {
    assessment: React.PropTypes.object.isRequired
};
const decoratedComponent = decorateComponent(AssessmentOverview, 'testManagementReducer');
export {decoratedComponent as AssessmentOverview};
