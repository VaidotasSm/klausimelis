import React from "react";
import {CommonStyles} from "../../../shared/common-styles";

function AssessmentQuestionsOverview({questions}) {
    function getAnswerStyle(question, answer) {
        const isSelected = answer.value === question.answer;
        if (isSelected && answer.correct) {
            return CommonStyles.selectedCorrectAnswerStyle;
        }
        if (isSelected && !answer.correct) {
            return CommonStyles.selectedIncorrectAnswerStyle;
        }
        return answer.correct ? CommonStyles.correctAnswerStyle : {};
    }

    return (
        <div className="list-group">
            {questions.map((question, i) =>
                <div key={i}>
                    <div>
                        <div className="list-group-item">
                            {question.name}
                        </div>
                        <div className="list-group-item">
                            <div className="row">
                                <div className="col-xs-6 col-sm-3">
                                    <span>Answers</span>
                                </div>
                                <div className="col-xs-6 col-sm-9 list-group">
                                    {question.answers.map((answer, i) =>
                                        <div key={i} className="row">
                                            <span style={getAnswerStyle(question, answer)}>
                                                {answer.value}
                                            </span>
                                        </div>
                                    )}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
}

AssessmentQuestionsOverview.propTypes = {
    questions: React.PropTypes.array.isRequired
};
export {AssessmentQuestionsOverview};

