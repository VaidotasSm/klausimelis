import React from "react";
import {AssessmentQuestionsOverview} from "./assessment-questions-overview.component";

class AssessmentView extends React.Component {
    constructor(props, context) {
        super(props, context);

        this.toAnsweredQuestions = this.toAnsweredQuestions.bind(this);
    }

    toAnsweredQuestions() {
        const assessment = this.props.assessment;
        const testVersion = assessment.submission.testVersion;
        const test = this.props.test;
        let testQuestions;
        if (test.current.version === testVersion) {
            testQuestions = [...test.current.questions];
        } else {
            let correctTest = test.archive.find(archive => archive.version === testVersion);
            testQuestions = [...correctTest.questions];
        }

        const assessmentQuestions = [... assessment.submission.questions];
        assessmentQuestions.forEach((assessmentQuestion, i) => {
            let testQuestion = testQuestions[i];
            assessmentQuestion.answers.forEach((aa, i) => {
                aa.correct = testQuestion.answers[i].correct;
            });
        });
        return assessmentQuestions;
    }

    render() {
        return (
            <div>
                <div className="row">
                    <div className="col-xs-4">
                        <label>Name</label>
                    </div>
                    <div className="col-xs-8">
                        <span>{this.props.assessment.name}</span>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-4">
                        <label>Status</label>
                    </div>
                    <div className="col-xs-8">
                        {this.props.assessment.submission.submitted ?
                            <span>Submitted</span> :
                            <span>Not submitted</span>
                        }
                    </div>
                </div>

                <div className="row">
                    <div className="col-xs-4">
                        <label>Test Version</label>
                    </div>
                    <div className="col-xs-8">
                        <span>{this.props.assessment.submission.testVersion}</span>
                    </div>
                </div>
                <div className="row">
                    <br />
                    <AssessmentQuestionsOverview questions={this.toAnsweredQuestions()}/>
                </div>
            </div>
        );
    }
}

AssessmentView.propTypes = {
    assessment: React.PropTypes.object.isRequired,
    test: React.PropTypes.object.isRequired
};
export {AssessmentView};
