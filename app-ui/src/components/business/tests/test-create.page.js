import React from "react";
import {connect} from "react-redux";
import {decorateComponent} from "../../../redux/component-decorator.js";
import {PageHeader} from "../../elements/page-header.component.js";
import {TestEdit} from "./elements/test-edit.component";
import {TestModel} from "./shared/model";
import {TestManagementActions} from "../../../redux/actions/test-management.actions";

class TestCreatePage extends React.Component {
    constructor(props, context) {
        super(props, context);

        const test = new TestModel();
        this.state = {test};
        this.onSaveClick = this.onSaveClick.bind(this);
    }

    onSaveClick() {
        this.props.dispatch(TestManagementActions.createTest(this.state.test));
    }

    render() {
        return (
            <div>
                <PageHeader text="New Test"/>
                <hr />
                <TestEdit test={this.state.test}/>

                <div className="row">
                    <div className="pull-right">
                        <button className="btn btn-default" onClick={this.onSaveClick}>Save</button>
                    </div>
                </div>
            </div>
        );
    }
}

const decoratedComponent = decorateComponent(TestCreatePage, 'testManagementReducer');
export {decoratedComponent as TestCreatePage};
