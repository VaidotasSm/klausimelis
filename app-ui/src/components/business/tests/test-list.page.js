import React from "react";
import {Link} from "react-router";
import {TestManagementActions} from "../../../redux/actions/test-management.actions.js";
import {decorateComponent} from "../../../redux/component-decorator.js";
import {CommonStyles} from "../../shared/common-styles.js";
import {PageHeader} from "../../elements/page-header.component.js";

class TestListPage extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.props.dispatch(TestManagementActions.getTests());
    }

    render() {
        return (
            <div>
                <PageHeader text="Tests"/>

                <div className="list-group">
                    {this.props.reducer.tests.map(test =>
                        <div key={test.id} className="list-group-item" style={CommonStyles.pointerCursorStyle}>
                            <Link to={`/private/tests/${test.id}/view`}>{test.name}</Link>
                        </div>
                    )}
                </div>
            </div>
        );
    }
}

const decoratedComponent = decorateComponent(TestListPage, 'testManagementReducer');
export {decoratedComponent as TestListPage};
