import React from "react";
import {decorateComponent} from "../../../redux/component-decorator";
import {TestManagementActions} from "../../../redux/actions/test-management.actions.js";
import {AssessmentModel} from "./shared/model";

class AssessmentCreatePage extends React.Component {

    constructor(props, context) {
        super(props, context);

        const testId = this.props.params.testId;
        this.state = {testId, test: this.props.reducer.test, assessment: new AssessmentModel('', testId)};
        if (!this.props.reducer.test) {
            this.props.dispatch(TestManagementActions.getTest(testId));
        }

        this.onValueChange = this.onValueChange.bind(this);
        this.onSave = this.onSave.bind(this);
    }

    componentWillReceiveProps(props) {
        if (!this.state.test) {
            this.setState(Object.assign({}, this.state, {test: props.reducer.test}));
        }
    }

    onValueChange(event) {
        const assessment = Object.assign({}, this.state.assessment, {[event.target.name]: event.target.value});
        this.setState(Object.assign({}, this.state, {assessment}));
    }

    onSave(event) {
        event.preventDefault();
        this.props.dispatch(TestManagementActions.createTestAssessment(this.state.assessment));
    }

    render() {
        const inputWidthStyle = {width: "80%"};

        return (
            <div>

                <div className="row">
                    <div className="col-xs-4"><label>Test name</label></div>
                    <div className="col-xs-4">{this.state.test ? this.state.test.name : ''}</div>
                </div>

                <form onSubmit={this.onSave} method="post">
                    <div className="row">
                        <div className="col-xs-4">
                            <label>Assessment Name</label>
                        </div>
                        <div className="col-xs-8">
                            <input type="text" style={inputWidthStyle}
                                   value={this.state.assessment.name}
                                   name="name" onChange={this.onValueChange}/>
                        </div>
                    </div>

                    <div className="row">
                        <div className="pull-right">
                            <input type="submit" className="btn btn-default" value="Create"/>
                        </div>
                    </div>
                </form>

            </div>
        );
    }

}

const decoratedComponent = decorateComponent(AssessmentCreatePage, 'testManagementReducer');
export {decoratedComponent as AssessmentCreatePage};

