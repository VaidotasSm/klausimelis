import React from "react";
import {CommonStyles} from "../shared/common-styles";

const PublicPageLayout = function (props) {
    const headerStyle = {color: "white"};

    return (
        <div>
            <nav className="navbar navbar-default">
                <div className="page-header text-center">
                    <h1 style={headerStyle}>Take your assessment</h1>
                </div>
            </nav>
            <div className="row">
                <div className={CommonStyles.mainWidthClass}>
                    {props.children}
                </div>
            </div>
        </div>
    );
};

export {PublicPageLayout};

