import React from "react";
import {Link} from "react-router";
import {CommonStyles} from "../shared/common-styles";

const MainPageLayout = function (props) {
    return (
        <div>
            <div>
                <nav className="navbar navbar-default">
                    <div className="container-fluid">
                        <ul className="nav navbar-nav navbar-left">
                            <li>
                                <Link to="/private/tests">Tests</Link>
                            </li>
                            <li>
                                <Link to="/private/tests/create">Create Test</Link>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <div className="row">
                <div className={CommonStyles.mainWidthClass}>
                    {props.children}
                </div>
            </div>
        </div>
    );
};

export {MainPageLayout};
