import React from "react";

const NotFoundPage = function (props) {
    return (
        <div>
            <h3>Page not found</h3>
        </div>
    );
};

export {NotFoundPage};
