import React from "react";
import ReactDom from "react-dom";
import {Router, Route, IndexRoute, IndexRedirect, hashHistory} from "react-router";
import {Provider} from "react-redux";
import {configureStore} from "./redux/redux-store";
import {MainPageLayout} from "./components/root/main-page-layout.component";
import {NotFoundPage} from "./components/root/not-found.page";
import {TestListPage} from "./components/business/tests/test-list.page";
import {TestCreatePage} from "./components/business/tests/test-create.page";
import {TestViewPage} from "./components/business/tests/test-view.page";
import {AssessmentCreatePage} from "./components/business/tests/assessment-create.page";
import {AssessmentParticipationPage} from "./components/business/assessment/assessment-participation.page";
import {PublicPageLayout} from "./components/root/public-page-layout";
import {AssessmentCompletedPage} from "./components/business/assessment/assessment-completed.page";

const store = configureStore();
ReactDom.render(
    <Provider store={store}>
        <Router history={hashHistory}>
            <Route path="/">
                <IndexRedirect to="/private"/>
                <Route path="/private" component={MainPageLayout}>
                    <IndexRoute component={TestListPage}/>
                    <Route path="/private/tests" component={TestListPage}/>
                    <Route path="/private/tests/create" component={TestCreatePage}/>
                    <Route path="/private/tests/:testId/view" component={TestViewPage}/>
                    <Route path="/private/tests/:testId/assessments/create" component={AssessmentCreatePage}/>
                </Route>
                <Route path="/public" component={PublicPageLayout}>
                    <Route path="/public/assessments/:testId/:assessmentId" component={AssessmentParticipationPage}/>
                    <Route path="/public/assessments/done" component={AssessmentCompletedPage}/>
                </Route>
                <Route path="*" component={NotFoundPage}/>
            </Route>
        </Router>
    </Provider>,
    document.getElementById('app')
);
